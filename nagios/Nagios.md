## NAGIOS

**Instal·lació:**
- sudo apt install nagios3..

**Obrir el navegador i posar:**
- localhost/nagios3

**Configuració**

- sudo cp /etc/nagios3/conf.d/localhost_nagios2.cfg /etc/nagios3/conf.d/(nom).cfg

 - sudo nano /etc/nagios3/conf.d/mail.cfg

![](imgs/20190405-134834.png)

![](imgs/20190405-135052.png)

>La ip del nostre servidor amb el servei del mail és la 192.168.10.5

- sudo nano /etc/nagios3/conf.d/ftp.cfg

![](imgs/20190405-135006.png)

>La ip del nostre servidor amb el servei del ftp és la 192.168.10.6

 - sudo nano /etc/nagios3/conf.d/web.cfg

![](imgs/20190405-134921.png)

>La ip del nostre servidor amb el servei del apache és la 192.168.10.7

- sudo nano /etc/nagios3/conf.d/squid.cfg

![](imgs/20190409-092944.png)

>La ip del nostre servidor amb el servei del proxy és la 192.168.1.1

- **sudo service nagios3 restart**

![](imgs/20190409-104306.png)
