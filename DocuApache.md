## Documentació apache2

**Instal·lació:**

    sudo apt install apache2
    
**Configuració:**

Primer de tot fem un cp del fitxer de configuració per defecte:

    sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/webcorporativaRAY.conf
    
Entrem en el fitxer de configuració i editem els següents parametres:

    ServerName www.webcorporativaRAY.com
    DocumentRoot /var/www/webcorporativaRAY
    
Crearem el document root:

    - cd /var/www
    - sudo chown -R www-data .
    - sudo mkdir webcorporativaRAY
    - cd webcorporativaRAY
    - sudo nano index.html

Dins del index.html posarem el contingut que volem que es mostri en aquest cas ficarem:

    - Hola benvinguts a la nostra web corporativa!


Com que volem que la nostra pagina web sigui segura crearem un altre fitxer de configuració amb ssl:

    sudo cp /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/webcorporativaRAY-ssl.conf
    
Ara crearem el certificat ssl, per aixó haurem de crear un directori i un fitxer:

    sudo mkdir /etc/apache2/ssl
    
Seguidament amb la següent comanda crearem el certificat:
    
    sudo make-ssl-cert /usr/share/ssl-cert/ssleay.cnf /etc/apache2/ssl/apache.pem
    
Després entrarem al fitxer de configuració webcorporativaRAY.conf i el configurarem:
    
    sudo nano /etc/apache2/sites-available/webcorporativaRAY-ssl.conf
    
Modificarem les següents linies:

    - ServerName www.webcorporativaRAY.com
    - DocumentRoot /var/www/webcorporativaRAY
    - SSLEngine on
    - SSLCertificateFile  /etc/apache2/ssl/apache.pem
    - SSLCertificateKeyFile /etc/apache2/ssl/apache.pem
    
Ara redireccionarem la nostra pàgina, es a dir, si busquen la nostra pagina amb http els redirigirà a https:
Primer de tot hem d'activar dos moduls:

    sudo a2enmod headers
    sudo a2enmod rewrite
    
Un cop hem activat aquests dos mòduls entrarem en el fitxer de configuracio del default i li afegirem les següents línies al final:

    <IfModule mod_rewrite.c>
      RewriteEngine On
      RewriteCond %{HTTPS} off
      RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
     </IfModule>
     
Un cop ja hem afegit les línias anteriors, entrarem en el fitxer de configuració del ssl i afegirem la següent línia al principi de tot:

    <IfModule mod_ssl.c>
    
Per fer les altres pàgines demanades seguirem el mateix procediment però amb el nom adient.

A la intranet seguirem el mateix procediment però farem alguns canvis.
En el fitxer de configuració del default afegirem les següents línies per crear una autenticació:

    <Directory "/var/www/intranet">
    AuthType Basic
    AuthName " Acces nomes a personal autoritzat"
    AuthUserFile /etc/apache2/passwd/.htpasswd
    Require user caps sysadmins
    </Directory>
    
I en el index.html de la intranet ficarem el següent contingut per a que quan cliquem en el link ens porti al nagios:

	<!DOCTYPE html>
	<html lang="es">
        	<head>
                	<meta charset="utf-8"/>
                	<title>intranetRAY</title>
        	</head>
        	<body>
                	<h1>Benvinguts a la IntranetRAY</h1>
                	<a href="http://192.168.10.50/nagios3/">Estadístiques i monitorització del nagios i serveis</a>
        	</body>
	</html>

Per accedir a la web ho farem:

    - http://www.webcorporativaRAY.com
    - http://www.botigavirtualRAY.com
    - http://www.intranetRAY.com
    
Per mirar els logs:

    - sudo tail -f /var/log/httpd
    
    
