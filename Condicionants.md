### Enunciat projecte: serveis per al treball amb clients

#### Context
 • Mirar apartat context.. 
#### Situació prèvia
- Ja esteu a la nova seu de l'empresa a Granollers, el CPD i la xarxa local està funcionant perfectament.

- Els treballadors de l'empresa disposen de correu corporatiu en un servidor situat al CPD de l'empresa

>Clonarem una mv d’un client Windows i un altre Ubuntu amb El Mail configurat en un servidor.

- Les empreses client disposen d'un espai accessible des de FTP amb programari situat en un servidor al CPD de l'empresa

>Clonarem una mv d’un client Windows i un altre Ubuntu amb El FTP configurat en un servidor.

#### Objectiu principal
- L'empresa vol oferir una pàgina web corporativa, una botiga virtual i una intranet que estaran al CPD de l'empresa.

>Farem 3 virtual host; una web corporativa, una botiga virtual i una intranet

- L'empresa vol assegurar l'accés a Internet dels usuaris sota una sèrie de condicionants.

>Farem certificats, SSL en tots els serveis i ficarem regles en el Firewall

- L'empresa vol que els treballadors puguin treballar de forma segura des de les empreses clients i des de casa accedint als serveis de l'empresa

>Per treballar de forma segura des de casa accedint als serveis de l’empresa, utilitzarem la VPN

- Es vol que els serveis públics no estiguin a la mateixa xarxa que els treballadors.

>Instal·larem i configurarem una DMZ  per separar la xarxa pública (Internet) de la LAN

#### Aquests són els condicionats:
- Des de Internet només s'ha de poder accedir als serveis que ofereix l'empresa i exclusivament per protocols segurs

>Per poder accedir a protocols segurs utilitzarem el SSL i afegirem regles en el Firewall per a que només permeti utilitzar protocols segurs

- La web corporativa ha de ser accessible per a tot Internet i des de la xarxa local.
>Per crear aquestes webs, hem de crear diferents Virtual Hosts per a cadascún i el servidor WEB haurà d’estar dins de la DMZ per a que els servei pugui ser accessible tant en LAN com per Internet
- Els altres serveis s'han de poder accedir des de la xarxa local i connexions remotes segures (només per part dels nostres treballadors).
>En aquest apartat tots els serveis que fem; DNS, FTP, MAIL... no els ficarem en la DMZ, els ficarem en la xarxa local per a que només vegin aquests serveis els treballadors de l’empresa
- Cal monitoritzar els serveis de xarxa de manera que rebem avís si algun servei no funciona.
>Per monitoritzar els serveis de xarxa utilitzarem el Nagios
- S'ha de definir una política de control d'accés a Internet (Web) i ha de ser diferent segons el càrrec que s'ocupa a l'empresa (cap, programadors, sysdamins, etc.).
>Per tenir controlat tot el trànsit que entra i surt de la nostra LAN utilitzarem un servidor Proxy per tenir aquest control
- A la intranet hi haurà les estadístiques d'us del proxy i els monitors dels serveis de la xarxa. Només hi podran accedir el cap i els sysadmin.
>La web intranet la farem amb un Virtual Host amb tota la informació que ens demana i a la configuració li afegirem la autenticació només per al cap i als sysadmins
- Els sysadmins han de poder administrar remotament els servidors.
>Utilitzant el protocol SSH, els sysadmins podran administrar remotament els servidors
- Per a resoldre incidències dels usuaris que es troben a les empreses client, els sysadmins han de poder administrar remotament els seus portàtils.
>Per poder fer aquesta funció de resoldre les incidencies remotament, instal·larem i configurarem el servei VPN amb el PfSense que ens permetrà estar lluny de l’empresa però a la vegada estar a la mateixa xarxa d’aquella empresa per solucionar aquest error
- Recorda que s'ha d'accedir als serveis per nom.
>Per accedir als serveis per nom, es a dir tenir una resolució de noms, tindrem un servidor DNS per fer aquesta funció
#### NOTES
- No cal fer funcional la pàgina web corporativa ni la botiga virtual, només una pàgina amb un missatge indicant el què és.

#### Documents a lliurar:
- Manuals d'utilització per als nostres treballadors
- Deixeu clar, ja sigui amb text, una taula o un esquema com s'han assolit cadascun dels requisits (tant a la documentació com al elevator pitch)

#### Conceptes clau
- Firewall
- DMZ
 - Proxy
- Servidor web
- Autenticació
- VPN
- Administració remota
