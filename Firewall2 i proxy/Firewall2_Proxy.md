### DOCUMENTACIÓ DEL FIREWALL2 I EL PROXY:

##### CONFIGURACIONS PRINCIPALS:

Primer configurem el nostre servidor PfSense amb les interfícies:

![](imgs/20190426-100825.png)

![](imgs/20190426-100829.png)

Iniciem la mv i li configurem les IP’s:
    • WAN (Adaptador Pont USB) = 192.168.10.2
    • LAN  (Xarxa Interna) = 192.168.1.1

![](imgs/20190426-100832.png)

Iniciem amb una mv Ubuntu Client amb Xarxa Interna

![](imgs/20190426-100836.png)

El client es configura per DHCP ja que Pfsense activa per defecte el servei DHCP per la interfície LAN

![](imgs/20190426-100840.png)

En la mv obrim el navegador i fiquem la IP de la nostra LAN (Xarxa Interna) del PfSense

![](imgs/20190426-100844.png)

##### CONFIGURACIÓ DEL FIREWALL2:
Anem a configurar una mica el Firewall2 per a que deixi passar el tràfic que nosaltres vulguem, tant per la LAN com LAN.
Anem a Interfaces → LAN i abaix de tot desactivem l’opció «Block private networks and loopback addresses», aquesta opció bloqueja per defecte tot el tràfic

![](imgs/20190426-100849.png)

Ara fem el mateix que abans però anem a Interfaces → WAN i desactivem la mateixa opció 

![](imgs/20190426-100853.png)

Anem a Sistema → Routing i afegim un gateway per defecte, en aquest cas fiquem la IP de la DMZ del Firewall extern

![](imgs/20190426-100858.png)

Per últim anem a Servicios → Servidor DHCP i ho configurem de la següent manera i és la configuració que obtindran els clients al iniciar

![](imgs/20190426-100901.png)

![](imgs/20190426-100904.png)

I anem a Servicios → Resolvedor DNS i afegim les dos webs que poden veure els clients

![](imgs/20190426-100911.png)

##### REGLES DEL FIREWALL2:

###### LAN:
Anem a Firewall → Rules i cliquem sobre la pestanya de LAN, per afegir regles sobre el tràfic que volem permetre o bloquejar que passi per el nostre firewall en aquest cas del tràfic que passa de la LAN a la DMZ:

    1. Després tenim una regla amb el protocol TCP/UDP d’IP i port d’origen qualsevol, amb IP destí de la LAN net amb el port de destí 22(SSH) 
    
![](imgs/20190426-100919.png)
       
    2. Totes les regles que tenim afegides són d’origen LAN net amb el protocol TCP/UDP per qualsevol port d’origen amb la IP de destí del servidor que ofereix el servei que li toca i amb el port de destí del servei que ofereix el servidor que toca:
    • IP = 192.168.10.5 / Servidor MAIL
    • IP = 192.168.10.6 / Servidor FTP
    • IP = 192.168.10.7 / Servidor WEB

![](imgs/20190426-100931.png)

    3. Ara tenim aquesta regla que ens deixarà navegar, té la xarxaLN A Net d’origen amb el protocol TCP, amb qualsevol port d’origen i qualsevol IP o xarxa de destí i per el port http i https(80/443)
       
![](imgs/20190426-100944.png)

![](imgs/20190426-100948.png)
       
    4. Per últim, tenim una regla amb IP d’origen LAN net amb protocol TCP per qualsevol port d’origen, i amb una IP de destí qualsevol pel protocol 53(DNS)
    
![](imgs/20190426-100956.png)

    5. Afegim una regla amb protocol TCP d’origen LAN Net amb qualsevol port origen, amb una LAN address de destí per el port 3128 (Proxy)
    
![](imgs/20190426-101006.png)

###### WAN (DMZ):
Anem a Firewall → Rules i cliquem sobre la pestanya de WAN, per afegir regles sobre el tràfic que volem permetre o bloquejar que passi per el nostre firewall en aquest cas del tràfic que passa de la DMZ a la LAN:

    1. De moment només tenim una regla per fer proves, que deixa passar tot de la WAN  a LAN per qualsevol xarxa i port d’origen i destí

![](imgs/20190426-101016.png)

##### INSTAL·LACIÓ DEL PROXY (SQUID I SQUIDGUARD):

Un cop dins de la Web del PfSense anem a System → Package Manager i ens descarreguem el Squid i SquidGuard, en el meu cas ja els tinc instal·lats

![](imgs/20190426-101020.png)

Per a descarregar els serveis hem d’anar a la pestanya Available Packages i busquem els serveis per nom

![](imgs/20190426-101024.png)

##### CONFIGURACIÓ DEL PROXY:

Començem la configuració del squid desprès de fer la instal·lació dels paquets, anem a Services → Squid Proxy Server, i haurem de configurar els següents paràmetres

![](imgs/20190426-101029.png)

![](imgs/20190426-101033.png)

Anem a la pestanya ACL’s i fiquem el següent:

![](imgs/20190426-101036.png)

Anem a la pestanya Users i afegim un per a cada lloc de treball

![](imgs/20190426-101040.png)

Ara configurem la autenticació d’aquests usuaris a la pestanya Authentication

![](imgs/20190426-101048.png)

Ara fem la configuració del SquidGuard, anem a Service → SquidGuard Proxy Filter
 
![](imgs/20190426-101053.png)

![](imgs/20190426-101058.png)

Anem a la pestanya Blacklist i ens descarreguem el paquet amb totes les pàgines prohibides d’entrar.
 
![](imgs/20190426-101105.png)

Anem a la pestanya de Common ACL i cliquem al + on fica Target Rules List i Cliquem a + i deneguem les categories «porn» i «searchengines» i per defecte fiquem allow, pe a que no bloqueji

![](imgs/20190426-101129.png)

![](imgs/20190426-101131.png)

Anem a la pestanya Groups ACL i afegim els diferents treballs que és fan a l’empresa

![](imgs/20190426-101134.png)

Dins de cada GroupACL ho configurem així, només haurem de canviar l’usuari client per el que toca en cada treball

![](imgs/20190426-101139.png)

Anem a la pestanya Target Categories i creem una i fiquem el següent:

![](imgs/20190426-101143.png)

![](imgs/20190426-101146.png)

En la pestanya CommonACL habilitem l’anterior Categoria que hem creat, per habilitar les nostres webs.

![](imgs/20190426-101149.png)

Anem a General Settings i apliquem els canvis que hem fet

![](imgs/20190426-101153.png)

##### COMPROVACIONS DEL FUNCIONAMENT PROXY:

![](imgs/20190426-101157.png)

![](imgs/20190426-101200.png)

![](imgs/20190426-101203.png)

![](imgs/20190426-101205.png)














