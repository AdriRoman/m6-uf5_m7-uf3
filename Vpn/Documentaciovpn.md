## VPN

Configura Firewall Regla
Per poder connectar-nos al servidor OpenVPN hem de obrir el port 1194 UDP en el nostre tallafocs . Ha de quedar com es mostra en la següent imatge:

![](imgs/20190409-083746.png)

##### Exportador de configuracions d'OpenVPN

Ara instal·larem l'exportador de configuracions d'OpenVPN, per a això entrarem al menú superior a System> Package Manager> Disponible Packages , buscarem " openvpn-client-export " i ho instal·larem.

![](imgs/20190409-084048.png)

##### Assistent configuració OpenVPN

Ja estem preparats per configurar OpenVPN, per això vam entrar al menú superior a VPN> OpenVPN    i entrarem en   Wizard . En tipus de servidor indicarem " Local User Access " i farem clic a " Next ". En aquest pas crearem una autoritat de certificació , per a això omplirem tots els camps que ens demanen i farem clic a " Add new CA ". Ara crearem un Certificat, per a això omplirem tots els camps que ens demanen i farem clic a " Create a new Certificate ". Ara procedirem a configurar el servei d'OpenVPN, deixarem tots els valors per defecte i omplirem "Tunnel Network "amb 10.0.8.0/24 i"

![](imgs/20190409-084543.png)

 Local Network "amb  192.168.1.0/24 
 
 ![](imgs/20190409-084614.png)
 
 i farem clic a" Next ". En el Servers en sortira una cosa com aixi:
 
 ![](imgs/20190409-084722.png)
 
##### Configuració de Certificats#####
Com hem fet servir l'assistent ja tindrem configurat el nostre certificat i CA. Només quedarà configurar la Revocació de Certificats, per a això entrarem al menú superior a   System> Certificate Manager> Certificate Revocation  i farem clic a " Add or Import CRL ". Deixem tots els camps omplerts com apareixen, l'important és que a "Certificate Authority" aparegui el que hem creat en l'assistent del pas anterior, per guardar els canvis farem clic a " Save" .

![](imgs/20190409-084935.png)

##### Crear usuaris per OpenVPN

Ara procedirem a crear usuaris perquè puguin entrar a la nostra VPN, per a això entrarem al menú superior a  System> User Manager  i farem clic al botó verd " + add ". Omplirem tots els camps activant l'opció " 

![](imgs/20190409-085100.png) ![](imgs/20190409-085147.png)

### Important!
Click to create a user certificate ".
![](imgs/20190409-085404.png)

##### Configuració OpenVPN
Ja finalment configurarem el nostre servidor OpenVPN perquè permeti autenticació mitjançant certificat i usuari. Per això entrarem al menú superior a  VPN> OpenVPN  i editarem el servidor que ens apareix. A l'apartat " Server mode " triarem  Remote Access (SSL / TLS + User Auth)  i guardarem els canvis. Ja tenim el nostre servidor OpenVPN configurat.

![](imgs/20190409-090038.png)

##### Exportar usuaris OpenVPN
Ara entrarem al menú superior a  VPN> OpenVPN i després anirem a Client Export  allà podrem descarregar-nos els corresponents arxius per configurar el client d'OpenVPN.

![](imgs/20190409-091036.png)
 
##### Configura client OpenVPN
Obrirem el nostre client d'OpenVPN i importarem el fitxer de configuració ovpn que hem descarregat en el pas anterior. Un cop importat ja podrem connectar-nos especificant l'usuari i contrasenya que especifiquem en passos anteriors.
![](imgs/20190409-101534.png) User Clients
![](imgs/20190409-091217.png) Cliquem al icona + que es troba al part de la dreta.
![](imgs/20190409-091338.png) Importa des d'un fitxer...
![](imgs/20190409-091443.png) Posem usuari i contrasenya.
![](imgs/20190409-091526.png) I finalment ens conectem 

