## Projecte M6 - M7
##### Raul Soler Adrià Román Yassin Sordou

#### Introducció

La situació prèvia del nostre projecte es que ja estem a la nova seu de l’empresa a Granollers, i el CPD i la xarxa local està funcionant perfectament.

Els nostres treballadors disposen de correu corporatiu en un servidor situtat al CPD de l’empresa. 

I també un servei de compartició de fitxers a través d'Internet per a quan els nostres treballadors viatgin a les empreses client puguin fer les instal·lacions i per a oferir als clients els nostres software propietari amb uns protocols de seguretat.
Així que el nostre objectiu és crear un servei proxy un servidor web.

#### Condicionants:

• Des de Internet només s'ha de poder accedir als serveis que ofereix l'empresa i exclusivament per protocols segurs 
Per poder accedir a protocols segurs utilitzarem el SSL i afegirem regles en el Firewall per a que només permeti utilitzar protocols segurs

• La web corporativa ha de ser accessible per a tot Internet i des de la xarxa local. 
Per crear aquestes webs, hem de crear diferents Virtual Hosts per a cadascún i el servidor WEB haurà d’estar dins de la DMZ per a que els servei pugui ser accessible tant en LAN com per Internet

• Els altres serveis s'han de poder accedir des de la xarxa local i connexions remotes segures (només per part dels nostres treballadors). 
En aquest apartat tots els serveis que fem; DNS, FTP, MAIL... no els ficarem en la DMZ, els ficarem en la xarxa local per a que només vegin aquests serveis els treballadors de l’empresa

• Cal monitoritzar els serveis de xarxa de manera que rebem avís si algun servei no funciona. 
Per monitoritzar els serveis de xarxa utilitzarem el Nagios

• S'ha de definir una política de control d'accés a Internet (Web) i ha de ser diferent segons el càrrec que s'ocupa a l'empresa (cap, programadors, sysdamins, etc.). 
Per tenir controlat tot el trànsit que entra i surt de la nostra LAN utilitzarem un servidor Proxy per tenir aquest control

• A la intranet hi haurà les estadístiques d'us del proxy i els monitors dels serveis de la xarxa. Només hi podran accedir el cap i els sysadmin. 
La web intranet la farem amb un Virtual Host amb tota la informació que ens demana i a la configuració li afegirem la autenticació només per al cap i als sysadmins

• Els sysadmins han de poder administrar remotament els servidors. 
Utilitzant el protocol SSH, els sysadmins podran administrar remotament els servidors

• Per a resoldre incidències dels usuaris que es troben a les empreses client, els sysadmins han de poder administrar remotament els seus portàtils. 
Per poder fer aquesta funció de resoldre les incidencies remotament, instal·larem i configurarem el servei VPN amb el PfSense que ens permetrà estar lluny de l’empresa però a la vegada estar a la mateixa xarxa d’aquella empresa per solucionar aquest error

• Recorda que s'ha d'accedir als serveis per nom. 
Per accedir als serveis per nom, es a dir tenir una resolució de noms, tindrem un servidor DNS per fer aquesta funció

#### FIREWALL2 I EL PROXY:

##### CONFIGURACIONS PRINCIPALS:

Primer configurem el nostre servidor PfSense amb les interfícies:

![](imgs/20190426-100825.png)

![](imgs/20190426-100829.png)

Iniciem la mv i li configurem les IP’s:
    • WAN (Adaptador Pont USB) = 192.168.10.2
    • LAN  (Xarxa Interna) = 192.168.1.1

![](imgs/20190426-100832.png)

Iniciem amb una mv Ubuntu Client amb Xarxa Interna

![](imgs/20190426-100836.png)

El client es configura per DHCP ja que Pfsense activa per defecte el servei DHCP per la interfície LAN

![](imgs/20190426-100840.png)

En la mv obrim el navegador i fiquem la IP de la nostra LAN (Xarxa Interna) del PfSense

![](imgs/20190426-100844.png)

##### CONFIGURACIÓ DEL FIREWALL2:
Anem a configurar una mica el Firewall2 per a que deixi passar el tràfic que nosaltres vulguem, tant per la LAN com LAN.
Anem a Interfaces → LAN i abaix de tot desactivem l’opció «Block private networks and loopback addresses», aquesta opció bloqueja per defecte tot el tràfic

![](imgs/20190426-100849.png)

Ara fem el mateix que abans però anem a Interfaces → WAN i desactivem la mateixa opció 

![](imgs/20190426-100853.png)

Anem a Sistema → Routing i afegim un gateway per defecte, en aquest cas fiquem la IP de la DMZ del Firewall extern

![](imgs/20190426-100858.png)

Per últim anem a Servicios → Servidor DHCP i ho configurem de la següent manera i és la configuració que obtindran els clients al iniciar

![](imgs/20190426-100901.png)

![](imgs/20190426-100904.png)

I anem a Servicios → Resolvedor DNS i afegim les dos webs que poden veure els clients

![](imgs/20190426-100911.png)

##### REGLES DEL FIREWALL2:

###### LAN:
Anem a Firewall → Rules i cliquem sobre la pestanya de LAN, per afegir regles sobre el tràfic que volem permetre o bloquejar que passi per el nostre firewall en aquest cas del tràfic que passa de la LAN a la DMZ:

    1. Després tenim una regla amb el protocol TCP/UDP d’IP i port d’origen qualsevol, amb IP destí de la LAN net amb el port de destí 22(SSH) 
    
![](imgs/20190426-100919.png)
       
    2. Totes les regles que tenim afegides són d’origen LAN net amb el protocol TCP/UDP per qualsevol port d’origen amb la IP de destí del servidor que ofereix el servei que li toca i amb el port de destí del servei que ofereix el servidor que toca:
    • IP = 192.168.10.5 / Servidor MAIL
    • IP = 192.168.10.6 / Servidor FTP
    • IP = 192.168.10.7 / Servidor WEB

![](imgs/20190426-100931.png)

    3. Ara tenim aquesta regla que ens deixarà navegar, té la xarxaLN A Net d’origen amb el protocol TCP, amb qualsevol port d’origen i qualsevol IP o xarxa de destí i per el port http i https(80/443)
       
![](imgs/20190426-100944.png)

![](imgs/20190426-100948.png)
       
    4. Per últim, tenim una regla amb IP d’origen LAN net amb protocol TCP per qualsevol port d’origen, i amb una IP de destí qualsevol pel protocol 53(DNS)
    
![](imgs/20190426-100956.png)

    5. Afegim una regla amb protocol TCP d’origen LAN Net amb qualsevol port origen, amb una LAN address de destí per el port 3128 (Proxy)
    
![](imgs/20190426-101006.png)

###### WAN (DMZ):
Anem a Firewall → Rules i cliquem sobre la pestanya de WAN, per afegir regles sobre el tràfic que volem permetre o bloquejar que passi per el nostre firewall en aquest cas del tràfic que passa de la DMZ a la LAN:

    1. De moment només tenim una regla per fer proves, que deixa passar tot de la WAN  a LAN per qualsevol xarxa i port d’origen i destí

![](imgs/20190426-101016.png)

##### INSTAL·LACIÓ DEL PROXY (SQUID I SQUIDGUARD):

Un cop dins de la Web del PfSense anem a System → Package Manager i ens descarreguem el Squid i SquidGuard, en el meu cas ja els tinc instal·lats

![](imgs/20190426-101020.png)

Per a descarregar els serveis hem d’anar a la pestanya Available Packages i busquem els serveis per nom

![](imgs/20190426-101024.png)

##### CONFIGURACIÓ DEL PROXY:

Començem la configuració del squid desprès de fer la instal·lació dels paquets, anem a Services → Squid Proxy Server, i haurem de configurar els següents paràmetres

![](imgs/20190426-101029.png)

![](imgs/20190426-101033.png)

Anem a la pestanya ACL’s i fiquem el següent:

![](imgs/20190426-101036.png)

Anem a la pestanya Users i afegim un per a cada lloc de treball

![](imgs/20190426-101040.png)

Ara configurem la autenticació d’aquests usuaris a la pestanya Authentication

![](imgs/20190426-101048.png)

Ara fem la configuració del SquidGuard, anem a Service → SquidGuard Proxy Filter
 
![](imgs/20190426-101053.png)

![](imgs/20190426-101058.png)

Anem a la pestanya Blacklist i ens descarreguem el paquet amb totes les pàgines prohibides d’entrar.
 
![](imgs/20190426-101105.png)

Anem a la pestanya de Common ACL i cliquem al + on fica Target Rules List i Cliquem a + i deneguem les categories «porn» i «searchengines» i per defecte fiquem allow, pe a que no bloqueji

![](imgs/20190426-101129.png)

![](imgs/20190426-101131.png)

Anem a la pestanya Groups ACL i afegim els diferents treballs que és fan a l’empresa

![](imgs/20190426-101134.png)

Dins de cada GroupACL ho configurem així, només haurem de canviar l’usuari client per el que toca en cada treball

![](imgs/20190426-101139.png)

Anem a la pestanya Target Categories i creem una i fiquem el següent:

![](imgs/20190426-101143.png)

![](imgs/20190426-101146.png)

En la pestanya CommonACL habilitem l’anterior Categoria que hem creat, per habilitar les nostres webs.

![](imgs/20190426-101149.png)

Anem a General Settings i apliquem els canvis que hem fet

![](imgs/20190426-101153.png)

##### COMPROVACIONS DEL FUNCIONAMENT PROXY:

![](imgs/20190426-101157.png)

![](imgs/20190426-101200.png)

![](imgs/20190426-101203.png)

![](imgs/20190426-101205.png)

#### VPN

Configura Firewall Regla
Per poder connectar-nos al servidor OpenVPN hem de obrir el port 1194 UDP en el nostre tallafocs . Ha de quedar com es mostra en la següent imatge:

![](imgs/20190409-083746.png)

##### Exportador de configuracions d'OpenVPN

Ara instal·larem l'exportador de configuracions d'OpenVPN, per a això entrarem al menú superior a System> Package Manager> Disponible Packages , buscarem " openvpn-client-export " i ho instal·larem.

![](imgs/20190409-084048.png)

##### Assistent configuració OpenVPN

Ja estem preparats per configurar OpenVPN, per això vam entrar al menú superior a VPN> OpenVPN    i entrarem en   Wizard . En tipus de servidor indicarem " Local User Access " i farem clic a " Next ". En aquest pas crearem una autoritat de certificació , per a això omplirem tots els camps que ens demanen i farem clic a " Add new CA ". Ara crearem un Certificat, per a això omplirem tots els camps que ens demanen i farem clic a " Create a new Certificate ". Ara procedirem a configurar el servei d'OpenVPN, deixarem tots els valors per defecte i omplirem "Tunnel Network "amb 10.0.8.0/24 i"

![](imgs/20190409-084543.png)

 Local Network "amb  192.168.1.0/24 
 
 ![](imgs/20190409-084614.png)
 
 i farem clic a" Next ". En el Servers en sortira una cosa com aixi:
 
 ![](imgs/20190409-084722.png)
 
##### Configuració de Certificats#####
Com hem fet servir l'assistent ja tindrem configurat el nostre certificat i CA. Només quedarà configurar la Revocació de Certificats, per a això entrarem al menú superior a   System> Certificate Manager> Certificate Revocation  i farem clic a " Add or Import CRL ". Deixem tots els camps omplerts com apareixen, l'important és que a "Certificate Authority" aparegui el que hem creat en l'assistent del pas anterior, per guardar els canvis farem clic a " Save" .

![](imgs/20190409-084935.png)

##### Crear usuaris per OpenVPN

Ara procedirem a crear usuaris perquè puguin entrar a la nostra VPN, per a això entrarem al menú superior a  System> User Manager  i farem clic al botó verd " + add ". Omplirem tots els camps activant l'opció " 

![](imgs/20190409-085100.png) ![](imgs/20190409-085147.png)

###### Important!
Click to create a user certificate ".
![](imgs/20190409-085404.png)

##### Configuració OpenVPN
Ja finalment configurarem el nostre servidor OpenVPN perquè permeti autenticació mitjançant certificat i usuari. Per això entrarem al menú superior a  VPN> OpenVPN  i editarem el servidor que ens apareix. A l'apartat " Server mode " triarem  Remote Access (SSL / TLS + User Auth)  i guardarem els canvis. Ja tenim el nostre servidor OpenVPN configurat.

![](imgs/20190409-090038.png)

##### Exportar usuaris OpenVPN
Ara entrarem al menú superior a  VPN> OpenVPN i després anirem a Client Export  allà podrem descarregar-nos els corresponents arxius per configurar el client d'OpenVPN.

![](imgs/20190409-091036.png)
 
##### Configura client OpenVPN
Obrirem el nostre client d'OpenVPN i importarem el fitxer de configuració ovpn que hem descarregat en el pas anterior. Un cop importat ja podrem connectar-nos especificant l'usuari i contrasenya que especifiquem en passos anteriors.

![](imgs/20190409-101534.png) 
User Clients
![](imgs/20190409-091217.png) 
Cliquem al icona + que es troba al part de la dreta.
![](imgs/20190409-091338.png) 
Importa des d'un fitxer...
![](imgs/20190409-091443.png) 
Posem usuari i contrasenya.
![](imgs/20190409-091526.png) 
I finalment ens conectem 


#### Apache2

**Instal·lació:**

    sudo apt install apache2
    
**Configuració:**

Primer de tot fem un cp del fitxer de configuració per defecte:

    sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/webcorporativaRAY.conf
    
Entrem en el fitxer de configuració i editem els següents parametres:

    ServerName www.webcorporativaRAY.com
    DocumentRoot /var/www/webcorporativaRAY
    
Crearem el document root:

    - cd /var/www
    - sudo chown -R www-data .
    - sudo mkdir webcorporativaRAY
    - cd webcorporativaRAY
    - sudo nano index.html

Dins del index.html posarem el contingut que volem que es mostri en aquest cas ficarem:

    - Hola benvinguts a la nostra web corporativa!


Com que volem que la nostra pagina web sigui segura crearem un altre fitxer de configuració amb ssl:

    sudo cp /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/webcorporativaRAY-ssl.conf
    
Ara crearem el certificat ssl, per aixó haurem de crear un directori i un fitxer:

    sudo mkdir /etc/apache2/ssl
    
Seguidament amb la següent comanda crearem el certificat:
    
    sudo make-ssl-cert /usr/share/ssl-cert/ssleay.cnf /etc/apache2/ssl/apache.pem
    
Després entrarem al fitxer de configuració webcorporativaRAY.conf i el configurarem:
    
    sudo nano /etc/apache2/sites-available/webcorporativaRAY-ssl.conf
    
Modificarem les següents linies:

    - ServerName www.webcorporativaRAY.com
    - DocumentRoot /var/www/webcorporativaRAY
    - SSLEngine on
    - SSLCertificateFile  /etc/apache2/ssl/apache.pem
    - SSLCertificateKeyFile /etc/apache2/ssl/apache.pem
    
Ara redireccionarem la nostra pàgina, es a dir, si busquen la nostra pagina amb http els redirigirà a https:
Primer de tot hem d'activar dos moduls:

    sudo a2enmod headers
    sudo a2enmod rewrite
    
Un cop hem activat aquests dos mòduls entrarem en el fitxer de configuracio del default i li afegirem les següents línies al final:

    <IfModule mod_rewrite.c>
      RewriteEngine On
      RewriteCond %{HTTPS} off
      RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
     </IfModule>
     
Un cop ja hem afegit les línias anteriors, entrarem en el fitxer de configuració del ssl i afegirem la següent línia al principi de tot:

    <IfModule mod_ssl.c>
    
Per fer les altres pàgines demanades seguirem el mateix procediment però amb el nom adient.

A la intranet seguirem el mateix procediment però farem alguns canvis.
En el fitxer de configuració del default afegirem les següents línies per crear una autenticació:

    <Directory "/var/www/intranet">
    AuthType Basic
    AuthName " Acces nomes a personal autoritzat"
    AuthUserFile /etc/apache2/passwd/.htpasswd
    Require user caps sysadmins
    </Directory>
    
I en el index.html de la intranet ficarem el següent contingut per a que quan cliquem en el link ens porti al nagios:

	<!DOCTYPE html>
	<html lang="es">
        	<head>
                	<meta charset="utf-8"/>
                	<title>intranetRAY</title>
        	</head>
        	<body>
                	<h1>Benvinguts a la IntranetRAY</h1>
                	<a href="http://192.168.10.50/nagios3/">Estadístiques i monitorització del nagios i serveis</a>
        	</body>
	</html>

Per accedir a la web ho farem:

    - http://www.webcorporativaRAY.com
    - http://www.botigavirtualRAY.com
    - http://www.intranetRAY.com
    
Per mirar els logs:

    - sudo tail -f /var/log/httpd
    
    
#### NAGIOS

**Instal·lació:**
- sudo apt install nagios3..

**Obrir el navegador i posar:**
- localhost/nagios3

**Configuració**

- sudo cp /etc/nagios3/conf.d/localhost_nagios2.cfg /etc/nagios3/conf.d/(nom).cfg

 - sudo nano /etc/nagios3/conf.d/mail.cfg

![](imgs/20190405-134834.png)

![](imgs/20190405-135052.png)

>La ip del nostre servidor amb el servei del mail és la 192.168.10.5

- sudo nano /etc/nagios3/conf.d/ftp.cfg

![](imgs/20190405-135006.png)

>La ip del nostre servidor amb el servei del ftp és la 192.168.10.6

 - sudo nano /etc/nagios3/conf.d/web.cfg

![](imgs/20190405-134921.png)

>La ip del nostre servidor amb el servei del apache és la 192.168.10.7

- sudo nano /etc/nagios3/conf.d/squid.cfg

![](imgs/20190409-092944.png)

>La ip del nostre servidor amb el servei del proxy és la 192.168.1.1

- **sudo service nagios3 restart**

![](imgs/20190409-104306.png)
